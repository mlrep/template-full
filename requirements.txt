scikit-learn~=0.22.1
pandas~=1.0.3
tensorboard_logger~=0.1.0
#mldev v0.4.dev0
mldev[base,dvc,bot,controller,tensorboard,jupyter]@https://gitlab.com/mlrep/mldev/-/package_files/24920143/download
